﻿//https://www.itunity.com/article/calling-office-365-apis-jquery-adaljs-2758

jQuery(function () {

    //authorization context
    var resource = 'https://outlook.office.com';
    var endpoint = 'https://outlook.office.com/api/v2.0/me/mailfolders/inbox/messages?$top=10';
    var calenderEndpoint = 'https://outlook.office.com/api/v2.0/me/calendarview?startDateTime=2016-01-18&endDateTime=2016-01-19';
    var postCalenderEndpoint = 'https://outlook.office.com/api/v2.0/me/events';
    var clientID = '28a707a5-0f11-4d93-8b88-6a918544da14';
    var tenantName = '365projectum.onmicrosoft.com';
    var authContext = new AuthenticationContext({
        instance: 'https://login.microsoftonline.com/',
        tenant: tenantName,
        clientId: clientID,
        postLogoutRedirectUri: window.location.origin,
        cacheLocation: 'localStorage'
    });

    //sign in and out
    jQuery("#signInLink").click(function () {
        authContext.login();
    });
    jQuery("#signOutLink").click(function () {
        authContext.logOut();
    });

    //save tokens if this is a return from AAD
    authContext.handleWindowCallback();
    authContext.handleWindowCallback();

    var user = authContext.getCachedUser();
    if (user) {  //successfully logged in

        //welcome user
        jQuery("#loginMessage").text("Welcome, " + user.userName);
        jQuery("#signInLink").hide();
        jQuery("#signOutLink").show();

        //call rest endpoint
        authContext.acquireToken(resource, function (error, token) {

            if (error || !token) {
                jQuery("#loginMessage").text('ADAL Error Occurred: ' + error);
                return;
            }

            // Get emails
            jQuery.ajax({
                type: 'GET',
                url: endpoint,
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token,
                },
            }).done(function (data) {

                
                jQuery('#restDataSubject').text(data.value[0].Subject);
                jQuery('#restDataTime').text(data.value[0].ReceivedDateTime);
                jQuery('#restDataSender').text(data.value[0].From.EmailAddress.Name);

                
                //jQuery("#loginMessage").text(JSON.stringify(data));
            }).fail(function (err) {
                jQuery("#loginMessage").text('Error calling REST endpoint: ' + err.statusText);
            }).always(function () {
            });

            

            // Get calendar events
            jQuery.ajax({
                type: 'GET',
                url: calenderEndpoint,
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token,
                },
                

            }).done(function (data) {
                jQuery("#restDataEventName").text(data.value[0].Subject);
                jQuery("#restDataEventStart").text(data.value[0].Start.DateTime + ' ' + data.value[0].Start.TimeZone);
                //alert(JSON.stringify(data));
                    
                }).fail(function(err) {
                jQuery("#loginMessage").text('Error calling REST endpoint: ' + err.statusText);
            }).always(function() {
            })
            ;

            var event = {
                "Subject": "Discuss the Calendar REST API",
                "Body": {
                    "ContentType": "HTML",
                    "Content": "I think it will meet our requirements!"
                },
                "Start": {
                    "DateTime": "2016-01-21T18:00:00",
                    "TimeZone": "Pacific Standard Time"
                },
                "End": {
                    "DateTime": "2016-01-21T19:00:00",
                    "TimeZone": "Pacific Standard Time"
                },
                "Attendees": [
                  {
                      "EmailAddress": {
                          "Address": "jesper@lundstocholm.dk",
                          "Name": "Janet Schorr"
                      },
                      "Type": "Required"
                  }
                ]
            };

            // Create calendar events
            jQuery.ajax({
                type: 'POST',
                url: postCalenderEndpoint,
                data: JSON.stringify(event),
                contentType: "application/json",
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ' + token,
                },

            }).done(function (data) {
                jQuery("#restDataEventName").text(data.value[0].Subject);
                jQuery("#restDataEventStart").text(data.value[0].Start.DateTime + ' ' + data.value[0].Start.TimeZone);
                //alert(JSON.stringify(data));

            }).fail(function (err) {
                jQuery("#loginMessage").text('Error calling REST endpoint: ' + err.statusText + '\n' + err.responseText);
            }).always(function () {
            })
            ;

        });

    }
    else if (authContext.getLoginError()) { //error logging in
        jQuery("#signInLink").show();
        jQuery("#signOutLink").hide();
        jQuery("#loginMessage").text(authContext.getLoginError());
    }
    else { //not logged in
        jQuery("#signInLink").show();
        jQuery("#signOutLink").hide();
        jQuery("#loginMessage").text("You are not logged in.");
    }

});